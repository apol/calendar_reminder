
#include <QCoreApplication>
#include <QDate>
#include <QFile>
#include <QString>
#include <QTextStream>

#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if (argc != 2) {
        std::cerr << "Wrong number of arguments" << std::endl;
        return 1;
    }

    QFile f(argv[1]);
    if (!f.exists()) {
        std::cerr << QString("File %1 does not exist").arg(argv[1]).toLocal8Bit().constData() << std::endl;
        return 2;
    }

    if (!f.open(QIODevice::ReadOnly)) {
        std::cerr << QString("File %1 could not be opened").arg(argv[1]).toLocal8Bit().constData() << std::endl;
        return 3;
    }

    QTextStream ts(&f);

    const QDate currentDate = QDate::currentDate();
    std::cout << QString::fromUtf8("Próximas reuniones de KDE España").toLocal8Bit().constData() << std::endl;
    bool anyMeeting = false;
    while (!ts.atEnd()) {
        const QString line = ts.readLine();
        const QString dateString = line.left(line.indexOf(' '));
        const QString rest = line.mid(line.indexOf(' ') + 1);
        const QDate date = QDate::fromString(dateString, Qt::ISODate);
        const int daysTo = currentDate.daysTo(date);
        if (daysTo >= 0 && daysTo <= 14) {
            std::cout << QString("\t * %1 - %2").arg(dateString).arg(rest).toLocal8Bit().constData() << std::endl;
            anyMeeting = true;
        }
    }
    if (!anyMeeting)
        std::cout << QString::fromUtf8("No hay reuniones en las próximas dos semanas").toLocal8Bit().constData() << std::endl;
    else
        std::cout << QString::fromUtf8("Recordad que estáis todos invitados (excepto a las de junta ;-))").toLocal8Bit().constData() << std::endl;
}

